(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){

var main =  angular.module('main',['ui.router','uiGmapgoogle-maps','ngGeolocation']);

    main.config(['$stateProvider', '$urlRouterProvider',
      function($stateProvider, $urlRouterProvider) {
        
        $urlRouterProvider.otherwise("/");

        // Now set up the states
        $stateProvider
          .state('home', {
            url: "/",
            templateUrl: "./home/home.view.html",
            controller: 'homeCtrl'
          })
          .state('location', {
            url: "/location/:id",
            templateUrl: "./location/location.view.html",
            controller: 'locationCtrl'
        })
      }]);
    
    var loc = function() {
      return {
        restrict: 'E',
        scope: {
          item: '=item'
        },
        templateUrl: './home/loc.html'
      };
    };
    
    main
    .controller('locCtrl',function($scope){
    
    })
    .directive('loc',loc);
    main
    .config(function(uiGmapGoogleMapApiProvider) {
        uiGmapGoogleMapApiProvider.configure({
                key: 'AIzaSyDVkresqu5NhmgmVZImr9E5n_6Yiadw9BY',
            v: '2.3.3', //defaults to latest 3.X anyhow
            libraries: 'weather,geometry,visualization'
        });
    });
},{}],2:[function(require,module,exports){
var main =  angular.module('main');

   var foursquare = function($http) {
        this.getLocations = function(map){
            return $http.get('https://api.foursquare.com/v2/venues/explore?client_id=HJYAW4G5FILENZNLJYTKLKJ5MOCVYD51ILNDVWKM0TPZ3ZB2&client_secret=YH3THYRMZQO55P2LDPVEXAMZ2UEYXDA10VFS0YV2AIFTM3AJ&v=20130815&ll='+ map.center.latitude +','+ map.center.longitude);
        }
        
        this.getLocation = function(id){
            return $http.get('https://api.foursquare.com/v2/venues/'+ id +'?client_id=HJYAW4G5FILENZNLJYTKLKJ5MOCVYD51ILNDVWKM0TPZ3ZB2&client_secret=YH3THYRMZQO55P2LDPVEXAMZ2UEYXDA10VFS0YV2AIFTM3AJ&v=20130815');
        }
    }
    
foursquare.$inject = ['$http'];
main.service('foursquare',foursquare);
},{}],3:[function(require,module,exports){
main =  angular.module('main');

   var homeCtrl = function($scope,foursquare,$geolocation) {
       //var vm = this;
            $scope.map = { center: { latitude: 30.693743, longitude: 76.842774 }, zoom:12};
          $geolocation.getCurrentPosition({
                timeout: 60000
             }).then(function(position) {
                //$scope.myPosition = position;
                console.log(position);
                $scope.map = { center: { latitude: position.coords.latitude, longitude: position.coords.longitude }, zoom: 12 };
                var map = $scope.map;
        
            foursquare.getLocations(map)
                .success(function (data) {
                    $scope.data = data.response.groups['0'].items;
                })
                .error(function (e) {
                    $scope.data = e;
                });
             });
          

    }
    
    
    homeCtrl.$inject = ['$scope','foursquare','$geolocation'];
    main.controller('homeCtrl',homeCtrl)
},{}],4:[function(require,module,exports){
main =  angular.module('main');


   var locationCtrl = function($scope,$stateParams,foursquare) {
       //var vm = this;
       var id = $stateParams.id;
       $scope.map = { center: { latitude: 30.693743, longitude: 76.842774 }, zoom:12};
        
        foursquare.getLocation(id)
            .success(function (data) {
                $scope.data = data.response.venue;
                 $scope.map = { center: { latitude: $scope.data.location.lat , longitude: $scope.data.location.lng }, zoom: 12 };
            })
            .error(function (e) {
                $scope.data = e;
            })
            
    }
    
    
    locationCtrl.$inject = ['$scope','$stateParams','foursquare'];
    main.controller('locationCtrl',locationCtrl)
},{}]},{},[1,3,4,2]);
