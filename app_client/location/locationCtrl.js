/*
 * Angular contoller for Location page
 */
main =  angular.module('main');

    // controller function unsing foursquare service and ui-router
   var locationCtrl = function($scope,$stateParams,foursquare) {
       
       var id = $stateParams.id;
       
       // set default map co-ords
       $scope.map = { center: { latitude: 30.693743, longitude: 76.842774 }, zoom:12};
        
        // use foursquare service to get info about a location using its id
        foursquare.getLocation(id)
            .success(function (data) {
                // set data to scope
                $scope.data = data.response.venue;
                 $scope.map = { center: { latitude: $scope.data.location.lat , longitude: $scope.data.location.lng }, zoom: 12 };
            })
            .error(function (e) {
                $scope.data = e;
            })
            
    }
    
    // wiring up controller to "main" module and injection for minification
    locationCtrl.$inject = ['$scope','$stateParams','foursquare'];
    main.controller('locationCtrl',locationCtrl)