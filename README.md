## Synopsis

A single page application featuring a map of your neighborhood you would like to visit.

## Live

[LocalSpot](https://quiet-ridge-11243.herokuapp.com/#/)

## Code Example

This web application is made using MEAN(MongoDb,Express,Angular,Node) stack.It uses web geolocation to identify location of user and then automatically uses that location to pull data from Foursquare API for respective popular locations in your vicinity.Similar to actual Foursquare Site.  

## Motivation

This application was made as a response to Udacity Front-End Web Developer Nanodegree Project: Neighborhood Map.

Bitbucket Repo: [Bitbucket](https://bitbucket.org/karanbali/localspot)

## License

MIT License